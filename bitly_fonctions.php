<?php
/**
 * Fonctions utiles au plugin Bitly
 *
 * @plugin     Bitly
 * @copyright  2015
 * @author     Perceval Archimbaud
 * @licence    GNU/GPL
 * @package    SPIP\Bitly\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/*
 * Un fichier de fonctions permet de définir des éléments
 * systématiquement chargés lors du calcul des squelettes.
 *
 * Il peut par exemple définir des filtres, critères, balises, …
 * 
 */

include_spip('inc/config');

/* Filtres */

function bitly($p)
{
	if (!lire_config('bitly/apikey'))
		return "Api Key non configurée.";
	$get_url = "https://api-ssl.bitly.com/v3/shorten?access_token=".lire_config('bitly/apikey')."&longUrl=".urlencode($p);
	$result = json_decode(file_get_contents($get_url));
	if (!$result->{'data'}->{'hash'})
		return "Erreur";
	return ("bit.ly/".$result->{'data'}->{'hash'});
}