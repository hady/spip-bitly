<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bitly_titre' => 'Bitly',

	// C
	'cfg_apikey' => 'API Key',
	'cfg_apikey_explication' => 'Paramètre requis. Indiquez ici votre clé d\'API Bitly disponible sur À COMPLETER.',
	'cfg_titre_parametrages' => 'Paramétrages',

	// T
	'titre_page_configurer_bitly' => 'Configuration du plugin Bitly',
);