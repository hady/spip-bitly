<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bitly_description' => 'Un plugin qui fournit un filtre pour raccourcir les URLs.',
	'bitly_nom' => 'Bitly',
	'bitly_slogan' => '',
);